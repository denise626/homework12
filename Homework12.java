import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Homework12 {

    public static void main(String [] args) {

        Set<String> cohort1 = new HashSet<String>();
        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("Ukraine");
        cohort1.add("Mexico");
        System.out.println(cohort1);

        Set<String> cohort2 = new HashSet<String>();
        cohort2.add("United States");
        cohort2.add("Canada");
        cohort2.add("United States");
        cohort2.add("Mexico");
        System.out.println(cohort2);

        Set<String> cohort3 = new HashSet<String>(cohort1);
        cohort3.addAll(cohort2);
        System.out.println(cohort3);

        Set<String> cohort4 = new TreeSet<>(cohort3);
        System.out.println(cohort4);

        Set<String> cohort5 = new HashSet<>(cohort1);
        cohort5.retainAll(cohort2);
        System.out.println(cohort5);

    }


}
